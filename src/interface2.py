#!usr/bin/env python3
"""
NAME:	    interface2.py
Purpose:	This module shows the interface required for the pipeline
Author:	 	Kimberley Kalkhoven & Sven Hakvoort
Created:	02/10/2017
version:	1.0
"""
from threading import Thread
from tkinter import *
from tkinter import ttk
from tkinter.filedialog import askdirectory, askopenfilename

from Processing import Processing

"""
The main class for the pipeline, builds the GUI and calls processes when the start button is pressed
"""
class Pipeline:
    def __init__(self, rootinput):
        self.root = rootinput
        # Show interface in fullscreen mode
        self.root.attributes("-fullscreen", True)

        # Variables
        self.input_dir = None
        self.output_dir = None
        self.graphmap_file = None
        self.reference_file = None
        self.index_file = None
        self.centrifuge_file = None
        self.start_processing = None
        self.processing = None
        self.mapping_line = ttk.Separator()

        # Labels
        self.label = Label(self.root)
        self.label.grid(row=3, column=2, columnspan=4)
        self.stop = Label(self.root)
        self.centrifuge_button = Button(self.root)
        self.reference_button = Button(self.root)
        self.graphmap_button = Button(self.root)
        self.index_button = Button(self.root)
        self.number_of_threads_title = Label(self.root)
        self.number_of_threads = Entry(self.root)

        self.process_option = StringVar()
        self.process_option.set("Not yet defined")

        self.centrifuge = IntVar()
        self.graphmap = IntVar()

        self.mapping_method = StringVar(self.root)
        self.mapping_method.set("Not yet defined")

        self.input_path = Label(self.root)
        self.input_path.grid(row=5, column=2, columnspan=3, sticky=W)
        self.output_path = Label(self.root)
        self.output_path.grid(row=6, column=2, columnspan=3, sticky=W)
        self.graphmap_path = Label(self.root)
        self.graphmap_path.grid(row=106, column=2, columnspan=3, sticky=W)
        self.reference_path = Label(self.root)
        self.reference_path.grid(row=111, column=2, columnspan=3, sticky=W)
        self.centrifuge_path = Label(self.root)
        self.centrifuge_path.grid(row=206, column=2, columnspan=3, sticky=W)
        self.index_path = Label(self.root)
        self.index_path.grid(row=211, column=2, columnspan=3, sticky=W)

        self.build_line(399, 6)
        self.start = Button(self.root, text="Start", command=self.start)
        self.start.grid(row=400, column=2, columnspan=2)

        self.build()

        self.root.mainloop()

    """
    Function to build the standard options
    """
    def build(self):
        # Text by real-time or batch button
        Label(self.root, text="Convert").grid(row=0, column=1, sticky=E)
        Label(self.root, text="Choose real-time or batch process").grid(row=1, column=2, columnspan=2, sticky=W)

        # Real time button
        real_time_button = Radiobutton(self.root, variable=self.process_option, text="Real time",
                                       command=self.real_time,
                                       value="True")
        real_time_button.grid(row=2, column=2, sticky=W)

        # Batch button
        batch_process_button = Radiobutton(self.root, variable=self.process_option, text="Batch", command=self.batch,
                                           value="False")
        batch_process_button.grid(row=2, column=3, sticky=W)

        self.build_assembly()
        self.build_mapping()
        self.build_taxonomic_identification()

    """
    Real time function to create a stop button. The stop button is provided to give the program a sign that the minion
    is finished.
    """
    def real_time(self):
        selection = "You selected the real time process build option (see button at bottom)"
        self.label.config(text=selection)
        self.equal_options()
        self.stop = Button(self.root, text='Minion finished', command=self.stop_watch_dog)
        self.stop.grid(row=400, column=4, columnspan=2)

    """
    Batch function to provide options for batch processing (options are equal to real_time, only real_time has one extra
    option)
    """
    def batch(self):
        self.stop.grid_remove()
        selection = "You selected the batch process build option"
        self.label.config(text=selection)
        self.equal_options()

    """
    Create buttons to select input and output directory. Are the same for real-time and batch.
    """
    def equal_options(self):
        # Input dir
        Button(self.root, text=' Select input directory', command=self.get_input_dir).grid(row=4, column=2,
                                                                                           columnspan=2, sticky=W)
        # Output dir
        Button(self.root, text=' Select output directory', command=self.get_output_dir).grid(row=4, column=4,
                                                                                             columnspan=2, sticky=W)

    """
    Get input directory and print path to user
    """
    def get_input_dir(self):
        self.input_dir = askdirectory(title="Select an input folder")
        if self.input_dir:
            self.input_path.config(text="Input directory: " + self.input_dir)

    """
    Get output directory and print path to user
    """
    def get_output_dir(self):
        self.output_dir = askdirectory(title="Select an output folder")
        if self.output_dir:
            self.output_path.config(text="Output directory: " + self.output_dir)

    """
    Get graphmap executable and print path to user
    """
    def get_graphmap(self):
        self.graphmap_file = askopenfilename(title="Select graphmap file")
        if self.graphmap_file:
            self.graphmap_path.config(text="Graphmap file: " + self.graphmap_file)

    """
    Get reference fasta file and print path to user
    """
    def get_reference(self):
        self.reference_file = askopenfilename(title="Select reference file",
                                              filetypes=[("All files", "*.*"), ("Fasta files", "*.fasta")])
        if self.reference_file:
            self.reference_path.config(text="Reference file: " + self.reference_file)

    """
    Get prefix index file and print path to user
    """
    def get_index(self):
        self.index_file = askopenfilename(title="Select prefix index file",
                                          filetypes=[("All files", "*.*"), ("Index files", "*.cf")])
        if self.index_file:
            self.index_path.config(text="Index file: " + self.index_file)

    def get_centrifuge(self):
        self.centrifuge_file = askopenfilename(title="Select centrifuge file")
        if self.centrifuge_file:
            self.centrifuge_path.config(text="Centrifuge file: " + self.centrifuge_file)

    def stop_watch_dog(self):
        Label(self.root, text="Minion is finished, remaining processes are started").grid(row=500, column=2)
        self.processing.stop_real_time_file_searcher()

    """
    Build assembly options
    """
    def build_assembly(self):
        self.build_line(10, 6)
        Label(self.root, text="\nAssembly\n\n\n").grid(row=11, column=1, sticky=E)
        Label(self.root, text="No suitable assembler found yet").grid(row=11, column=2, columnspan=2, sticky=E)

    """
    Build mapping options
    """
    def build_mapping(self):
        self.build_line(99, 6)
        Label(self.root, text="\nMapping").grid(row=100, column=1, sticky=E)
        # Checkboxes mapping methods
        Checkbutton(self.root, text="Graphmap", variable=self.graphmap).grid(row=101, column=2, sticky=W)
        # OUD Checkbutton(self.root, text="Centrifuge", variable=self.centrifuge).grid(row=101, column=3, sticky=W)

        # OptionMenu(self.root, self.mapping_method, "GraphMap", "Centrifuge") \
        #     .grid(row=101, column=2, sticky=W)
        Button(self.root, text="Select", command=self.get_mapping_method).grid(row=101, column=4)

    """
    Build taxonomic identification options
    """
    def build_taxonomic_identification(self):
        self.build_line(300, 6)
        Label(self.root, text="\nTaxonomic \nidentification").grid(row=301, column=1, sticky=E)
        Checkbutton(self.root, text="Centrifuge", variable=self.centrifuge).grid(row=302, column=2, sticky=W)
        Button(self.root, text="Select", command=self.get_taxonomic_identification).grid(row=302, column=4)

    """
    Start main program containing whole pipeline
    """
    def start(self):
        Label(self.root, text="Start pipeline...").grid(row=401, column=2, columnspan=4,
                                                        sticky=W)
        if self.process_option.get() == "True":
            real_time = True
        else:
            real_time = False
        gui_dict = {"input_dir": self.input_dir,
                    "output_dir": self.output_dir,
                    "graphmap_file": self.graphmap_file,
                    "reference_file": self.reference_file,
                    "index_file": self.index_file,
                    "centrifuge_file": self.centrifuge_file,
                    "number_of_threads": self.number_of_threads.get()}
        self.processing = Processing()
        self.start_processing = Thread(target=self.processing.start, args=[gui_dict, real_time, ])
        self.start_processing.start()

    """
    Build specific options for the chosen mapping method
    """
    def get_mapping_method(self):
        # Remove all buttons
        self.graphmap_button.grid_remove()
        self.reference_button.grid_remove()
        self.centrifuge_button.grid_remove()
        self.index_button.grid_remove()
        self.number_of_threads.grid_remove()
        self.number_of_threads_title.grid_remove()
        self.mapping_line.grid_remove()

        if self.graphmap.get() == 1 and self.centrifuge.get() == 1:
            self.build_graphmap()
            self.mapping_line = ttk.Separator(self.root, orient=HORIZONTAL)
            self.mapping_line.grid(row=150, column=2, columnspan=4, sticky="ew")
            self.build_centrifuge()

        if self.graphmap.get() == 1 and self.centrifuge.get() == 0:
            self.build_graphmap()

        if self.centrifuge.get() == 1 and self.graphmap.get() == 0:
            self.build_centrifuge()

    """
    Builds taxonomic identification options
    """
    def get_taxonomic_identification(self):
        self.centrifuge_button.grid_remove()
        self.index_button.grid_remove()
        self.number_of_threads.grid_remove()
        self.number_of_threads_title.grid_remove()

        if self.centrifuge.get() == 1:
            self.build_centrifuge()

    """
    Build graphmap options
    """
    def build_graphmap(self):
        # Select graphmap executable
        self.graphmap_button = Button(self.root, text='Select graphmap file', command=self.get_graphmap)
        self.graphmap_button.grid(row=105, column=2, columnspan=2, sticky=W)

        # Select reference file
        self.reference_button = Button(self.root, text='Select reference', command=self.get_reference)
        self.reference_button.grid(row=110, column=2, columnspan=2, sticky=W)

    """
    Build centrifuge options
    """
    def build_centrifuge(self):
        # Select centrifuge file
        self.centrifuge_button = Button(self.root, text='Select centrifuge file',
                                        command=self.get_centrifuge)
        self.centrifuge_button.grid(row=305, column=2, columnspan=2, sticky=W)

        # Select prefix index
        self.index_button = Button(self.root, text='Select one prefix file', command=self.get_index)
        self.index_button.grid(row=310, column=2, columnspan=2, sticky=W)

        # Give number of threads
        self.number_of_threads_title = Label(self.root, text="Number of threads")
        self.number_of_threads_title.grid(row=314, column=2, columnspan=2, sticky=W)
        self.number_of_threads = Entry(self.root, bd=1, text="Number of threads")
        self.number_of_threads.grid(row=315, column=2, columnspan=2, sticky=W)

    def build_line(self, row, columnspan):
        ttk.Separator(self.root, orient=HORIZONTAL).grid(row=row, columnspan=columnspan, sticky="ew")


if __name__ == "__main__":
    gui = Tk()
    gui.attributes("-fullscreen", True)
    Pipeline(gui)
