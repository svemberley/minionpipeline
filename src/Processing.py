#!/usr/bin/env python3
"""
NAME:	    processing.py
Purpose:	This class calls the main processing steps with the given arguments
Author:	 	Kimberley Kalkhoven & Sven Hakvoort
Created:	02/10/2017
version:	1.0
"""
import subprocess
import time

from CreatedFAST5 import CreatedFAST5
from Poretools import Poretools
from StopButton import StopButton
from centrifuge import Centrifuge
from mapper import Mapper
from watchdog.observers import Observer


class Processing:

    def __init__(self):
        self.poretools = Poretools()
        self.mapper = ''
        self.centrifuge = ''

    """Main controller for all the processes"""
    def start(self, gui_dict, realtime):
        # Dict: input_dir, output_dir, graphmap_file, reference_file, index_file, centrifuge_file, number_of_threads
        # Process real-time or batch -> convert fast5 files to fastq file
        if realtime:
            self.real_time_is_true(gui_dict)
        else:
            self.poretools.convert_to_fastq(gui_dict["input_dir"], gui_dict["output_dir"])

        # Create key value for locations fastq and fasta
        gui_dict["fastq"] = gui_dict["output_dir"] + gui_dict["input_dir"].split("/")[-1] + ".fastq"
        gui_dict["fasta"] = gui_dict["output_dir"] + gui_dict["input_dir"].split("/")[-1] + ".fasta"

        # Graphmap
        if gui_dict["graphmap_file"] is not None:
            self.mapper = Mapper(self.fastq_to_fasta(gui_dict["fastq"]),
                                 gui_dict["reference_file"],
                                 gui_dict["graphmap_file"])

        # Centrifuge
        if gui_dict["centrifuge_file"] is not None:
            if gui_dict["number_of_threads"] != "":
                self.centrifuge = Centrifuge(gui_dict["fastq"], gui_dict["index_file"],
                                             gui_dict["centrifuge_file"], gui_dict["number_of_threads"])
            else:
                self.centrifuge = Centrifuge(gui_dict["fastq"], gui_dict["index_file"], gui_dict["centrifuge_file"])

        return 0

    """If real time processing is checked, use watchdog listener"""
    def real_time_is_true(self, gui_dict):
        event = CreatedFAST5(gui_dict["output_dir"])
        self.observer = Observer()
        self.observer.schedule(event, gui_dict["input_dir"], recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(1)
        except StopButton:
            self.observer.stop()
        self.observer.join()

    """Convert a FASTQ file to FASTA"""
    def fastq_to_fasta(self, fastq):
        out = fastq.strip(".")[0]+".fasta"
        subprocess.call("sed -n '1~4s/^@/>/p;2~4p'", fastq, ">", out)
        return out

    """Stop the watchdog listener"""
    def stop_real_time_file_searcher(self):
        print("observer uit door stopbutton")
        self.observer.stop()


if __name__ == "__main__":
    test = {'input_dir': '/Users/kimberley/Documents/Bio-informatica/Periode09/Thema09/fast5',
            'output_dir': '/Users/kimberley/Documents/Bio-informatica/Periode09/Thema09/test',
            'graphmap_file': "", 'reference_file': "",
            'index_file': "", 'centrifuge_file': "", 'number_of_threads': ''}

    processor = Processing()
    processor.start(test, True)
