#! usr/bin/env python3
"""
NAME:	    poretools.py
Purpose:	This class converts FAST5 files to FASTQ
Author:	 	Kimberley Kalkhoven & Sven Hakvoort
Created:	02/10/2017
version:	1.0
"""
import os
import subprocess


class Poretools:
    def __init__(self):
        self.output = None

    """
    Converts fast5 data to fastq data
    :param input_path: path to file or directory containing fast5 data
    :param output_path: path to output directory
    :return: output: path to data (file or directory?)
    """
    def convert_to_fastq(self, input_path, output_path):
        if os.path.isfile(input_path):
            if self.output is None:
                self.output = output_path + "/" + input_path.split("/")[-2] + ".fastq"
        elif os.path.isdir(input_path):
            self.output = output_path + "/" + input_path.split("/")[-1] + ".fastq"
        subprocess.call(" ".join(['poretools', 'fastq', input_path, '>>', self.output]), shell=True)
