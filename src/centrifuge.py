#!/usr/bin/env python3
"""
NAME:	    centrifuge.py
Purpose:	This class calls the taxonomic identification process with the given arguments
Author:	 	Kimberley Kalkhoven & Sven Hakvoort
Created:	02/10/2017
version:	1.0
"""
import os
import subprocess
import sys


class Centrifuge:

    def __init__(self, file, database, centrifugePath, threads=1):
        self.file = self.setFile(file)
        self.database = self.setFile(database).split(".")[0]
        self.executablePath = centrifugePath
        self.threads = self.setThreads(threads)
        self.out = self.setOutDir("centrifuge_output")
        self.start()

    """Sets the number of threads"""
    def setThreads(self, threads):
        return int(threads)

    """Checks if outputdir exists, else creates it"""
    def setOutDir(self, out):
        if not os.path.exists(out):
            os.makedirs(out)
        return out

    """Checks if the input file exists"""
    def setFile(self, file):
        if os.path.isfile(file):
            return file
        else:
            raise IOError("File cannot be opened, check if the file exists and you have the right permissions")

    """Starts the process"""
    def start(self):
        args = [self.executablePath, "-q", "-p", self.threads, "-x", self.database, "-S",
                os.path.join(self.out, "classification_results.txt", self.file),
                "--report-file", os.path.join(self.out, "centrifuge_report.tsv")]
        subprocess.call(" ".join(args), shell=True, executable="/bin/bash")


def main(argv=None):
    if argv is None:
        argv = sys.argv
    test = Centrifuge(argv[1], argv[2], "centrifuge")
    return 0


if __name__ == "__main__":
    sys.exit(main())
