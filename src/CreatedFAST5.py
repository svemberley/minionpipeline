#!/usr/bin/env python3
"""
NAME:	    CreatedFAST5.py
Purpose:	The class which is called when the watchdog listener is triggered
Author:	 	Kimberley Kalkhoven & Sven Hakvoort
Created:	02/10/2017
version:	1.0
"""
import os

from Poretools import Poretools
from watchdog.events import FileSystemEventHandler


class CreatedFAST5(FileSystemEventHandler):

    def __init__(self, output):
        self.output_path = output

    """Function called by watchdog"""
    def dispatch(self, event):
        if event.is_directory:
            return
        filepath, ext = os.path.splitext(getattr(event, "src_path"))
        if ext == ".fast5":
            Poretools().convert_to_fastq(getattr(event, "src_path"), self.output_path)
            print(filepath, ext)
