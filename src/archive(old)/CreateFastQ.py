#! usr/bin/env pyton3

"""
CreateFastQ.py
Convert FAST5 files to FastQ files.
Creates FastQ files from obtained data minion (FAST5 files)
Should get possibility to create fastQ files in real-time or when the Minion
is done.
There is an option to choose a folder of  a single file to convert.
"""

import os
import time
import argparse
import subprocess
from watchdog.observers import Observer
from watchdog.events import FileCreatedEvent
from watchdog.events import FileSystemEventHandler


class CreatedHandler(FileSystemEventHandler):
    def on_created(self, event):
        if event.is_directory:
            return
        filepath, ext = os.path.splitext(event.src_path)
        if ext == ".fast5":
            print(filepath, ext)
            return CreateFastQ(os.path())


class CreateFastQ:

    def __init__(self, path):
        self.filename = ""
        self.realTime = False
        self.path = path
        self.arg_parse()
        self.get_files()

    def arg_parse(self):
        parser = argparse.ArgumentParser(
            description="Script for converting a FAST5 file into a FastQ file."
        )
        parser.add_argument("-d", "--Data", required=True, help="Path to FAST5 directory/file")
        parser.add_argument("-rt", "--RealTime", default=False, help="Option to process files at real time")
        args = parser.parse_args()
        self.filename = args.Data
        self.realTime = args.Realtime

    def get_files(self):
        filenames = []
        if self.realTime:
            if not os.path.isdir(os.path.join(os.getcwd(), self.filename)):
                raise Exception("By real time processing the selected path must be a directory")

        if os.path.isfile(self.filename):
            self.poretools(self.filename)
        elif os.path.isdir(os.path.join(os.getcwd(), self.filename)):
            # for filename in os.listdir(self.filename):
            #     filenames.append(os.path.join(os.getcwd(), self.filename, filename))
            print(os.path.join(os.getcwd(), self.filename))


if __name__ == "__main__":
    event_handler = CreatedHandler()
    observer = Observer()
    observer.schedule(event_handler, '.', recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
