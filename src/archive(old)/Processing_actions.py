#! usr/bin/env python3

import os
import subprocess


class ProcessingActions:

    def poretools(self, input_user):
        """
        Converts fast5 data to fastq data
        :param input_user: file or directory containing fast5 data
        :return: output: path to data (file or directory?)
        """

        output = os.path.join(os.getcwd(), os.path.splitext(input_user), ".fastq")

        # TODO  Choose to store all processed files in 1 file or make each time a new file.
        subprocess.call(['poretools', 'fastq', input_user, '>', output])
        return output

    def trimmer(self, output_poretools, threshold=20):
        """
        Quality control on fastq files and trims bad sequences
        :param output_poretools: path to fastq files
        :param threshold: threshold number to know where to trim sequences
        :return:
        """
        # Get filename with path excluded
        output_poretools = path.split("/")[-1]
        if not os.path.exists("output/"):
            os.makedirs("output/")
        # Open file and call awk in subprocess to remove hits smaller than 24
        with open("output/"+filename.split(".")[0] + "_out1." + filename.split(".")[1], 'w') as output_1:
            subprocess.call(['some trimmer', path], stdout=output_1)
        # Open output file of awk and sort with a subprocess call