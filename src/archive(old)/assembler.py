#!/usr/bin/env python3
"""
NAME:	    assembler.py
Purpose:	This class calls the assembler process with the given arguments
Author:	 	Kimberley Kalkhoven & Sven Hakvoort
Created:	02/10/2017
version:	1.0
"""
import os
import subprocess
import sys

"""
The main class for the assembly, requires a file and an output dir plus required arguments
"""
class Assembler:

    def __init__(self, file, assembler="megahit", outputdir="megaHitOutput", kMin="s", kMax=201, kStep=10):
        self.file = self.setFile(file)
        self.assembler = self.setAssembler(assembler)
        self.outPutDir = self.setOutDir(outputdir)
        self.kMin = self.setK(kMin)
        self.kMax = self.setK(kMax)
        self.kStep = self.setK(kStep)
        self.callAssembler()

    """
    Sets the k value to a string
    """
    def setK(self, kVal):
        return str(kVal)

    """
    Checks if the path to file is correct
    """
    def setFile(self, file):
        if os.path.isfile(file):
            return file
        else:
            raise IOError("File cannot be opened, check if the file exists and you have the right permissions")

    """
    Sets the type of assembler
    """
    def setAssembler(self, assembler):
        if assembler.lower() in ["abyss", "idba", "megahit"]:
            return assembler.lower()
        elif assembler.lower() == "spades" or assembler == "spades.py":
            return "spades.py"
        else:
            raise ValueError("Please enter a valid assembly method. Possible methods are:\nSPAdes\nIDBA\nAbyss")

    """
    Checks if the output dir exists, if not it creates a new dir
    """
    def setOutDir(self, out):
        if os.path.isdir(out):
            return out
        else:
            try:
                os.makedirs(out)
                return out
            except IOError:
                raise IOError("Could not create dir, make sure you have write permissions in the current directory")

    """
    Validate the given flags
    """
    def validateFlags(self):
        validFlags = []
        for flag in self.flags:
            if flag.startswith("--"):
                validFlags.append(flag)
        return validFlags

    """
    Calls a subprocess to start the assembly
    """
    def callAssembler(self):
        if self.assembler == "megahit":
            args = [self.assembler, "-r", self.file, "-o", self.outPutDir, self.kMin, self.kMax, self.kStep]
            subprocess.call(" ".join(args), shell=True, executable="/bin/bash")
        return self.outPutDir


def main(argv=None):
    if argv is None:
        argv = sys.argv
    test = Assembler(argv[1])
    return 0


if __name__ == "__main__":
    sys.exit(main())
