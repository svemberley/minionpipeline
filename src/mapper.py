#!/usr/bin/env python3
"""
NAME:	    mapper.py
Purpose:	This class calls the mapping process with the given arguments
Author:	 	Kimberley Kalkhoven & Sven Hakvoort
Created:	02/10/2017
version:	1.0
"""
import os
import subprocess
import sys


class Mapper:

    def __init__(self, file, reference, graphmap_executable):
        self.file = self.setFile(file)
        self.reference = self.setFile(reference)
        self.out = "output.sam"
        self.ex = graphmap_executable
        self.start()

    """Checks if the input file exists"""
    def setFile(self, file):
        if os.path.isfile(file):
            return file
        else:
            raise IOError("File cannot be opened, check if the file exists and you have the right permissions")

    """Starts the mapping process and converts the resulting SAM TO BAM, sorts """
    def start(self):
        args = [self.ex, "align -r", self.reference, "-d", self.file, "-o", self.out]
        subprocess.call(" ".join(args), shell=True, executable="/bin/bash")

        # SAM to BAM
        bam = self.out.split(".")[0]+".bam"
        args = ["samtools view -Sb", self.out, "-o", bam]
        subprocess.call(" ".join(args), shell=True, executable="/bin/bash")

        # Sort BAM
        args = ["samtools sort", bam, ">", bam]
        subprocess.call(" ".join(args), shell=True, executable="/bin/bash")

        # Index BAM
        args = ["samtools index", bam]
        subprocess.call(" ".join(args), shell=True, executable="/bin/bash")
        return bam


def main(argv=None):
    if argv is None:
        argv = sys.argv
    test = Mapper(argv[1], argv[2], argv[3])
    return 0


if __name__ == "__main__":
    sys.exit(main())
